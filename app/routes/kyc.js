var router = require('express').Router();
var kycController = new (require('../controllers/kyc.js'))();

router.get('/', kycController.renderKYCList);
router.get('/:kyc_id', kycController.renderKYC);

router.get('/:user_id/:kyc_id/approve', kycController.approveKYC);
router.get('/:user_id/:kyc_id/reject', kycController.rejectKYC);

module.exports = router