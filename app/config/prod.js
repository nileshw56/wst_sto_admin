module.exports = {
    host : 'http://localhost:5600',
    serverPort : 5600,
    db : {
        //aws
        //nilesh56/nileshw56
        mysql : {
            host            : "sto.cwbsabhe4ogh.ap-south-1.rds.amazonaws.com",
            user            : "nilesh56",
            password        : "nileshw56",
            database        : "sto"
        },
        mongoDB : {
            url     : 'mongodb://localhost:27017',
            db      : 'hc'
        }
    },
    session : {
        secret: 'nilesh',
        cookie: {}
    },
    error : {
        msg : {
            500 : "Internal server error"
        }
    },
    uploads : {
        kyc : 'uploads/kyc'
    },
    apiCurrency : {
        apiUrl : "https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest",
        apiKey : "f8f4410d-cfb0-49d7-9368-6abc3b2a73f6"
    },
    tokenPrice : 0.1,
    referral : {
        amount : {
            BTC : 0.001
        }
    }
};