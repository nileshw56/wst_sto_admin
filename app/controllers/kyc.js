var modelUsers = new (require('../models/users.js'))();
var Base = require('./base.js');

class KYC extends Base {
    renderKYCList(req, res) {
        var qr = 'select u.email, k.* from users u, kyc k where u.user_id = k.user_id and u.status = "KYC_ADDED" order by k.updated';    

        app.db.mysql.query(qr, function(err, results){
            console.log("adfaf",results);
            
            var viewData = {
                kyc : results,
                msg : req.query.msg,
                user : req.session.user
            };

            res.render('kyclist', viewData);

        });
    }

    renderKYC(req, res) {
        modelUsers.fetch('kyc', '*', {kyc_id:req.params.kyc_id}, null, null, null, function(err, results){
            if(err) {
                return res.redirect('/');
            }
            var kycData = results[0];
            kycData.gender = kycData.gender == 'M' ? 'Male' : 'Female';
            
            var viewData = {
                kyc : kycData,
                msg : req.query.msg,
                user : req.session.user
            };

            res.render('kyc', viewData);
        });    
    }

    approveKYC(req, res) {
        var kycId = req.params.kyc_id;
        var userId = req.params.user_id;

        console.log('asdfasf', kycId, userId);

        app.lib.async.auto({
            
            fetchUser : function(icb) {
                modelUsers.fetch('users', '*', {user_id:userId}, null, null, null, icb);
            },
            
            //change status of the user
            changeUserStatus : ["fetchUser", function(results, icb){
                modelUsers.update('users', {status:'ACTIVE'}, {user_id:userId}, icb);
            }],
            
            //change status of the KYC
            changeKYCStatus: ["fetchUser", function(results, icb){
                modelUsers.update('kyc', {status:'APPROVED'}, {kyc_id:kycId}, icb);
            }],

            //send user a mail
            sendMail : ["fetchUser", function(results, icb){
                console.log("iupfadsf", results);
                console.log("798u8hh", results.fetchUser);
                console.log("ppo89", results.fetchUser[0]);
                var mailOptions = {
                    from: 'info@wharfstreettechnologies.com',
                    to: results.fetchUser[0][0]['email'],
                    subject: 'Your KYC has been Approved!',
                    html: '<p>Dear User, <br><br><br>Your KYC Has been approved. You can start buying token. To buy token please logout and login again in the application for security purpose. <br><br><br>You can also you use below link to buy tokens<br><br><a href="'+app.config.userHost+'/logout"><h3>Click Here</h3></a><br><br><br>Thanks<br>Team WharfStreet</p>'
                };
    
                app.lib.mailer.sendMail(mailOptions, icb);
            }]
        }, function(err){

            console.log("eraer", err);
            res.redirect('/kyc')
        });
    }

    rejectKYC(req, res) {
        var kycId = req.params.kyc_id;
        var userId = req.params.user_id;

        app.lib.async.auto({
            
            fetchUser : function(icb) {
                modelUsers.fetch('users', '*', {user_id:userId}, null, null, null, icb);
            },
            
            //change status of the KYC
            changeKYCStatus: ["fetchUser", function(results, icb){
                modelUsers.update('kyc', {status:'REJECTED'}, {kyc_id:kycId}, icb);
            }],

            //send user a mail
            sendMail : ["fetchUser", function(results, icb){
                console.log("Asdfadsf", results);
                console.log("Asdf", results.fetchUser);
                console.log("Asdf", results.fetchUser[0]);
                var mailOptions = {
                    from: 'info@wharfstreettechnologies.com',
                    to: results.fetchUser[0][0]['email'],
                    subject: 'Your KYC has been Rejected!',
                    html: '<p>Dear User, <br><br><br>Your KYC Has been Rejected. Please login to wharfstreettechnologies.com and submit KYC again.<br><br><br>Thanks<br>Team WharfStreet</p>'
                };
    
                app.lib.mailer.sendMail(mailOptions, icb);
            }]
        }, function(err){
            res.redirect('/kyc')
        });
    }
}

module.exports = KYC;